FROM node:16

WORKDIR /usr/src/app

COPY scripts/* .

CMD ["node", "botdiscord.js"]
