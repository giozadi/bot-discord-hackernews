require('dotenv').config();
const { Client, Intents } = require('discord.js');
const bot = new Client({ intents: [Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_MESSAGES] });
const TOKEN = process.env.TOKEN;

const axios = require('axios');
const cheerio = require("cheerio");

bot.login(TOKEN);

bot.on('ready', () => {
  bot.channels.fetch("983374843904921630").then(channel => {
    channel.messages.fetch({ limit: 1 }).then(messages => {
      axios
      .get("https://thehackernews.com/")
      .then((response) => {
        const $ = cheerio.load(response.data);
        const featuredArticles = $(".story-link");
          let postLinkWrapper = $(featuredArticles[0])[0],
            postLink = $(postLinkWrapper);

          let lastNew = postLinkWrapper.attribs.href; // get url last new on the hacker news website;
      messages.forEach(message => 
        {
          if (lastNew != message.content) { // you can choose to filter for one specified channel;
            channel.send(lastNew)
          }
        })
    })
  })
})
})
 
