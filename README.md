# Bot Discord - HackerNews Last News

A bot to get last news every minute with K8S Crontab, Node.js & Axios.

## Getting started

# Demo

![Minikube Dashboard](/images/bot.png)

# Prerequisites:

You will need the following tools installed on your machine:

* [Kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
* [Minikube](https://kubernetes.io/docs/tutorials/hello-minikube/)
* [Docker](https://www.docker.com/get-started)

# Process:

Firstly, start Minikube:

```console
$ minikube start
```

Confirm that its started with:

```console
$ minikube status
```

Go into the minikube docker environment:

```console
$ eval $(minikube docker-env)
```

build then run the dockerfile, if running from the current diretory you just need to do the following (note i tagged my image botdiscord:

```console
$ docker build . -t botdiscord

$ docker run -it botdiscord
```

Then use kubectl to generate a template job and pipe the output to job.yaml (note that this is already done, but if you want to do yourself then feel free to do so):

```console
$ kubectl create cronjob botdiscord --image=botdiscord --schedule="*/5 * * * *" --dry-run=client -o=yaml > job.yaml
```

Apply the job:
```console
$ kubectl apply -f job.yaml 
```

Wait a few seconds and then confirm that the job is being applied with command:

```console
kubectl get cronjobs
```

If you require it you can verify that the job is running through the ui by typing in the following:

```console
minikube dashboard --url=true
```

Then copying and pasting the output into your browser to view the cron jobs applied.

<h3><b>Don't forget to create your .env file with your Discord Bot Token!</h3></b>

Jobe Done!

